FROM starefossen/ruby-node:alpine

RUN apk add --update \
  build-base \
  libxml2-dev \
  libxslt-dev \
  && rm -rf /var/cache/apk/*

RUN mkdir /project
WORKDIR /project

COPY Gemfile /project/Gemfile
COPY Gemfile.lock /project/Gemfile.lock
RUN bundle install

COPY package.json /project/package.json
COPY yarn.lock /project/yarn.lock
RUN yarn install

COPY . /project