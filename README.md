# Munimeetups

A sample web app to search for Meetups' groups in Munich, Germany.

See requirements in [challenge.md](challenge.md).

**Tech word soup:** Ruby 2.5, Rails 5, React, sidekiq, redis caching, docker, test automation, no persistence.


## Running

 1 . Create a file called `.env` in the root directory with your credentials to the Meetups' API:
```
$ cat > .env
MEETUP_SECRET_KEY=2b434054568f54910657431f56226b
```

 2 . Then `$ docker-compose up` in the root directory.

 3. Visit localhost:3000 to search for meetups in Munich area.

## Running specs

`$ docker-compose run background rspec`

## Features

 * Bookmarkable URL's
 * Asynchronous job processing
 * 100% test coverage (in the backend)
