class SearchsController < ApplicationController
  def show
    @search_query = params[:search_query]
    results = SearchGroup.for(@search_query)

    respond_to do |format|
      format.json do
        if results
          render json: { status: 'completed', results: results }
        else
          render json: { status: 'pending' }
        end
      end
      format.html { render 'show' }
    end
  end
end
