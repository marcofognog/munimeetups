import React, {Fragment} from "react"
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

class LoadingBar extends React.Component {
  render () {
    return (
      <Fragment>
        <Typography variant="body1">
        Loading results ...
        </Typography>
      </Fragment>
    );
  }
}

export default LoadingBar
