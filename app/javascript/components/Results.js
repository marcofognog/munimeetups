import React from "react"
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

class Results extends React.Component {
  render (){
    if(this.props.groups.length === 0) {
      return (<Typography component="p" variant="body1">No results to display.</Typography>);
    } else {
      return (<div>
        {this.props.groups.map((g, i) => {
        return (
            <Paper key={i} style={{padding: "10px", margin: "5px"}}>
            <img src={g.image} style={{display: "inline-block"}}/>
            <div style={{display: "inline-block", width: "400px", paddingLeft: "10px"}}>
              <Typography component="h5" variant="h6">{g.name} (<a href={g.url}>visit</a>)</Typography>
              <Typography component="p">{g.category}</Typography>
              <Typography component="p">{g.status}</Typography>
              <Typography component="p">{g.location}</Typography>
            </div>
            </Paper>
        );
      })}
      </div>
           );
    }
  }
}

export default Results
