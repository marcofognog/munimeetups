import React from "react"
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import LoadingBar from './LoadingBar'
import Results from './Results';

let axios = require('axios')

function DisplayResults({loading, groups}){
  if(loading){
    return (<LoadingBar />);
  }else{
    return (<Results groups={groups}/>);
  }
}

function Header(){
  return (<React.Fragment>
      <Typography variant="display2" >
        Munimeetups
      </Typography>
        <Typography variant="body2" >
        Search for Meetups groups in Munich, Germany
      </Typography>
          <br/>
         </React.Fragment>);
}

class SearchGroup extends React.Component {
  constructor(props){
    super();
    this.state = {
      query: props.search_text || "",
      loading: false,
      groups: [],
    };
    if(this.state.query !== ""){
      this.request();
    }
  }

  request = () => {
    axios.get(`/?search_query=${this.state.query}`,{headers: { "Accept": "application/json"}})
      .then((response)=>{
        if(response.data.status === "completed"){
          this.setState({loading: false, groups: response.data.results});
        } else {
          setTimeout(this.request, 1000);
          console.log('pending... trying again later.')
          this.setState({loading: true});
        }
      })
      .catch(error => {
        console.log('Error calling backend service.');
      });
  }

  handleSubmit = (e) => {
    this.setState({loading: true})
    this.request();
    history.pushState({}, "title", `?search_query=${this.state.query}`);
    e.preventDefault();
  }

  handleChange = (e) => {
    this.setState({query: e.target.value});
  }

  render () {
    return (
        <React.Fragment>
        <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      style={{ minHeight: '20vh' }}
        >
        <Grid item xs={3}>
        <Header />
        <form onSubmit={this.handleSubmit}>
        <TextField
      placeholder="Subject"
      margin="normal"
      name="search_query"
      value={this.state.query}
      onChange={this.handleChange}
        />
        <br/>
        <Button type="submit">Searh</Button>
        <br/>
        </form>
        </Grid>
        <DisplayResults loading={this.state.loading} groups={this.state.groups} />
        </Grid>
        </React.Fragment>
    );
  }
}

export default SearchGroup
