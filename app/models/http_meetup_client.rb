class HTTPMeetupClient
  @request_count = 0

  def self.get(querystring)
    @request_count += 1
    res = HTTParty.get(querystring)
    { code: res.code, body: JSON.parse(res.body) }
  end

  class << self
    attr_reader :request_count
  end
end
