class MockMeetupClient
  class << self
    attr_accessor :request_count
  end

  @request_count = 0

  def self.get(uri)
    require 'cgi'
    require 'uri'
    query = CGI.parse(URI.parse(uri).query)['text'].first

    code = query == 'cobol' ? 500 : 200

    body = [
      { 'category' =>
        {
          'id' => 34,
          'name' => 'Tech',
          'shortname' => 'tech',
          'sort_name' => 'Tech'
        },
        'city' => 'München',
        'country' => 'DE',
        'created' => 1_370_808_069_000,
        'description' =>
        '<p>We are the Munich Ruby user group yada yada ...</p>',
        'group_photo' =>
        { 'base_url' => 'https://secure.meetupstatic.com',
          'highres_link' =>
          'https://secure.meetupstatic.com/photos/6/3/highres_459985187.jpeg',
          'id' => 459_985_187,
          'photo_link' =>
          'https://secure.meetupstatic.com/photos/6/2/6/3/600_459985187.jpeg',
          'thumb_link' =>
          'https://domain.com/thumb.jpeg',
          'type' => 'event' },
        'id' => 8_851_722,
        'join_mode' => 'open',
        'key_photo' =>
        { 'base_url' => 'https://secure.meetupstatic.com',
          'highres_link' =>
          'https://secure.meetupstatic.com/photos/3/c/highres_247452332.jpeg',
          'id' => 247_452_332,
          'photo_link' =>
          'https://secure.meetupstatic.com/photos/3/0/2/c/600_247452332.jpeg',
          'thumb_link' =>
          'https://secure.meetupstatic.com/photos/3/0/2/c/thumb_247452332.jpeg',
          'type' => 'event' },
        'lat' => 48.14,
        'link' => 'https://www.meetup.com/Munich-Rubyshift-Ruby-User-Group/',
        'localized_country_name' => 'Germany',
        'localized_location' => 'München, Germany',
        'lon' => 11.58,
        'members' => 457,
        'meta_category' =>
        { 'category_ids' => [34],
          'id' => 292,
          'name' => 'Tech',
          'photo' =>
          { 'base_url' => 'https://secure.meetupstatic.com',
            'highres_link' =>
            'https://secure.meetupstatic.com/photos/highres_450131949.jpeg',
            'id' => 450_131_949,
            'photo_link' =>
            'https://secure.meetupstatic.com/photos/a/d/600_450131949.jpeg',
            'thumb_link' =>
            'https://secure.meetupstatic.com/photos/d/thumb_450131949.jpeg',
            'type' => 'event' },
          'shortname' => 'tech',
          'sort_name' => 'Tech' },
        'name' => 'Munich Rubyshift, Ruby User Group',
        'next_event' =>
        { 'id' => 'drpzdgyxpbsb',
          'name' => 'Ruby User Group Meetup',
          'time' => 1_542_218_400_000,
          'utc_offset' => 3_600_000,
          'yes_rsvp_count' => 4 },
        'organizer' =>
        { 'bio' => '',
          'id' => 31_318_442,
          'name' => 'Sebastian Schulze',
          'photo' =>
          { 'base_url' => 'https://secure.meetupstatic.com',
            'highres_link' =>
            'https://secure.meetupstatic.com/photos/0/highres_31622112.jpeg',
            'id' => 31_622_112,
            'photo_link' =>
            'https://secure.meetupstatic.com/photos4/0/member_31622112.jpeg',
            'thumb_link' =>
            'https://secure.meetupstatic.com/photos/8/4/0/thumb_31622112.jpeg',
            'type' => 'member' } },
        'score' => 4533.0,
        'state' => '',
        'status' => 'active',
        'timezone' => 'Europe/Berlin',
        'untranslated_city' => 'München',
        'urlname' => 'Munich-Rubyshift-Ruby-User-Group',
        'visibility' => 'public',
        'who' => 'Rubyists' }
    ]
    @request_count += 1
    { code: code, body: body }
  end
end
