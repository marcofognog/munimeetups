class SearchGroup
  def self.for(search_text)
    cached = Redis.new.get(search_text)

    if cached
      content = JSON.parse(cached)
      return content
    else
      GroupFetcherWorker.perform_async(search_text)
      return false
    end
  end
end
