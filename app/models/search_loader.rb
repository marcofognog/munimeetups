class SearchLoader
  class WebServiceError < StandardError; end

  MEETUP_ENDPOINT = 'https://api.meetup.com'.freeze
  FIND_GROUPS_PATH = '/find/groups'.freeze

  def self.load_search_job(search_text)
    result = fetch(search_text)
    if result[:code] == 200
      content = serialize(result[:body]).to_json
      Redis.new.set(search_text, content)
    else
      raise WebServiceError, "Unsuccessful service call returned: #{result}"
    end
  end

  def self.fetch(search_text)
    page = 10
    target_lat = '48.13999938964844'
    target_lon = '11.579999923706055'
    country = 'de'

    querystring = "photo-host=public&page=#{page}&text=#{search_text}"
    querystring += "&country=#{country}&lon=#{target_lon}&lat=#{target_lat}&"
    querystring += "sign=true&key=#{Rails.configuration.meetup_secret_key}"
    target = "#{MEETUP_ENDPOINT}#{FIND_GROUPS_PATH}?#{querystring}"
    Rails.logger.info("Requesting ----> #{target}")

    Rails.configuration.meetup_client.get(target)
  end

  def self.serialize(raw)
    raw.map do |group|
      {
        name: group['name'],
        image: group.dig('group_photo', 'thumb_link'),
        category: group['category']['name'],
        status: group['status'],
        location: group['localized_location'],
        url: group['link']
      }
    end
  end
end
