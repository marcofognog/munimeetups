class GroupFetcherWorker
  include Sidekiq::Worker

  sidekiq_options retry: 5

  sidekiq_retry_in do |count|
    2 * count
  end

  def perform(search_text)
    # to make the loading behavior more evident
    sleep 2

    SearchLoader.load_search_job(search_text)
  end
end
