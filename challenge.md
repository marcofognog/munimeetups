## Background

The exercise is to build a Ruby-on-rails application that will let users to search for meetups in Munich.

We will use Meetup's API to search for meetups in Munich and show past events' information for those meetups.
You will have to read the documentation for the API endpoints mentioned in the resources section and implement your solution
accordingly.

We assume that the Meetups' APIs are slow and that they take, in some cases, upto 1 minute to give the result.
With that in mind, we decide an application for user to first submit a search query and wait until the results
are availble as they fetched from the Meetups' API in the background.

## Resources

* Meetups' API documentation for the following two resources
  * Search for meetup groups: https://secure.meetup.com/meetup_api/console/?path=/find/groups
  * You might have to login or signup to be able to access the APIs
* To make API calls, you may use any Ruby HTTP library (or the built-in library).
* All API calls to the Meetups' API are to happen asynchronously (i.e., in background jobs). Use any libraries to achieve this asynchronous functionality.
* Use Rails version > 5.1
* Feel free to use JQuery or any other javascript library.
* There is no need to store anything in the database but feel free to use if needed.
* Designs provided along with this exercise are only for better understanding. Please do not spend much time on
the views. If you just print the plain text, its fine too. We focus only on functionality and code quality,
not on design.

## Features
* TriggerSearch:
  * User should be able to visit the homepage of the app and enter a query `text` in a search bar
  to search for meetups in Munich (location can be hardcoded to be Munich).
    * The form submit can happen either via Javascript or with page reload.
    * A Rails controller receives this request, triggers a background job for Meetup search and returns with a
    message *Request is being processed and results will be available shortly*.
    * The following should happen in the background job.
      * An API call is made to the Meetups' API.
      * Results are fetched and temporarily stored in a cache store (use [Redis](https://github.com/redis-store/redis-rails)).
* Polling for results:
  * Once the TriggerSearch is completed, the front-end starts polling for the search results and a `loading...`
  text is shown to the user.
  * Polling requests are made by javascript.
  * A separate endpoint receives these polling requests and checks if the results are
  available for the search.
    * If the results are not yet available, it returns no results and the front-end reads this and continues polling.
    * If the results are available, it returns the results.
* DisplayResults:
  * Once the results are returned, the user is presented with a list of meetups that matched their query.
    * Can be just a single page table with top 10 matches wihtout any pagination.
    * Show the meetup's `thumbnail photo`, `name`, `location`, `status`, `category`.
* User can enter new query text in the search field and search for new meetups again.

#### Caching
* Lets assume that the meetups API is expensive to make very frequent calls. To prevent ourselves
from additional costs, we would like to cache the queries for 5 minutes.
* So, if a user searches for the same query text within 5 minutes, we can retrieve the information
from the cache instead of making an API call.
* Use [Redis](https://github.com/redis-store/redis-rails) for caching.

#### Testing
* Test the application using rspec whenever you can.

## Guidelines
* Please add comments in code explaining the decisions that you have taken whenever necessary.
* Look for parts of code that needs refactoring and refactor them before submitting the solution.
