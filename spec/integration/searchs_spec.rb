require 'rails_helper'

describe '/', type: :request do
  before do
    Redis.new.flushall
  end

  it 'returns "peding" when cache is cold' do
    expected = {
      'status' => 'pending'
    }
    get('/?search_query=ruby', params: { format: :json })
    expect(JSON.parse(response.body)).to eq(expected)
  end

  it 'returns results successfully after cache warms' do
    seach_text = 'ruby'
    expected = {
      'status' => 'completed',
      'results' => [
        {
          'name' => 'Munich Rubyshift, Ruby User Group',
          'image' => 'https://domain.com/thumb.jpeg',
          'category' => 'Tech',
          'status' => 'active',
          'location' => 'München, Germany',
          'url' => 'https://www.meetup.com/Munich-Rubyshift-Ruby-User-Group/'
        }
      ]
    }
    get("/?search_query=#{seach_text}", params: { format: :json })

    # background job runs:
    SearchLoader.load_search_job(seach_text)

    get("/?search_query=#{seach_text}", params: { format: :json })
    expect(JSON.parse(response.body)).to eq(expected)
  end
end
