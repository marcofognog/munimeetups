require 'rails_helper'

describe SearchLoader do
  before do
    Redis.new.flushall
  end

  it 'searchs by group in Munich' do
    result = SearchLoader.fetch('ruby')
    expected = [
      {
        name: 'Munich Rubyshift, Ruby User Group',
        image: 'https://domain.com/thumb.jpeg',
        category: 'Tech',
        status: 'active',
        location: 'München, Germany',
        url: 'https://www.meetup.com/Munich-Rubyshift-Ruby-User-Group/'
      }
    ]

    expect(SearchLoader.serialize(result[:body])).to eq(expected)
  end

  it 'loads cache properly' do
    client = Rails.configuration.meetup_client
    expected_result = [
      {
        'name' => 'Munich Rubyshift, Ruby User Group',
        'image' => 'https://domain.com/thumb.jpeg',
        'category' => 'Tech',
        'status' => 'active',
        'location' => 'München, Germany',
        'url' => 'https://www.meetup.com/Munich-Rubyshift-Ruby-User-Group/'
      }
    ]

    redis = Redis.new
    expect do
      SearchLoader.load_search_job('ruby')
    end.to change { client.request_count }.by(1)
    expect(JSON.parse(redis.get('ruby'))).to eq(expected_result)
  end

  it 'raises error when the request is unsuccessful' do
    expect do
      SearchLoader.load_search_job('cobol')
    end.to raise_error(SearchLoader::WebServiceError)
  end
end
